package com.sabre.reader;

public class ReaderFactory {

    public Reader createReader(String pathToFile) {
        if (pathToFile.contains("http")) {
            return new InternetFileReader(pathToFile);
        }
        return new LocalFileReader(pathToFile);
    }

}
