package com.sabre.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class InternetFileReader extends BaseFileReader {

    private String url;

    public InternetFileReader(String url) {
        this.url = url;
    }

    @Override
    public BufferedReader createBufferedReader() throws IOException {
        return new BufferedReader(new InputStreamReader(new URL(url).openStream()));
    }
}
