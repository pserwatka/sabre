package com.sabre.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LocalFileReader extends BaseFileReader {

    private String pathToFile;

    public LocalFileReader(String pathToFile) {
        this.pathToFile = pathToFile;
    }


    @Override
    public BufferedReader createBufferedReader() throws IOException {
        return new BufferedReader(new FileReader(getClass().getClassLoader().getResource(pathToFile).getFile()));
    }
}
