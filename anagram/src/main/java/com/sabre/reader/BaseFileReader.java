package com.sabre.reader;

import com.sabre.anagram.context.AnagramContext;
import com.sabre.processor.JobProcessor;

import java.io.BufferedReader;
import java.io.IOException;

public abstract class BaseFileReader implements Reader {

    @Override
    public AnagramContext process(JobProcessor jobProcessor) {
        try (BufferedReader br = createBufferedReader()) {
            String currentLine;
            AnagramContext contextProcessor = new AnagramContext();

            while ((currentLine = br.readLine()) != null) {

                jobProcessor.startProcess(contextProcessor, currentLine);

            }
            return contextProcessor;
        } catch (IOException e) {
            throw new RuntimeException("Exception wile processing data", e);
        }
    }

    public abstract BufferedReader createBufferedReader() throws IOException;
}
