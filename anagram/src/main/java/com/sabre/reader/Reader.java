package com.sabre.reader;

import com.sabre.processor.ContextProcessor;
import com.sabre.processor.JobProcessor;

public interface Reader {
    ContextProcessor process(JobProcessor jobProcessor);
}
