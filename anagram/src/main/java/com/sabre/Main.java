package com.sabre;

import com.sabre.anagram.context.AnagramContext;
import com.sabre.anagram.process.BuildWordMapProcessor;
import com.sabre.anagram.process.FindLongestAnagramProcessor;
import com.sabre.anagram.process.FindLongestSetOfAnagramsProcessor;
import com.sabre.anagram.process.SortWordProcessor;
import com.sabre.processor.ContextProcessor;
import com.sabre.processor.JobProcessor;
import com.sabre.processor.JobProcessor.JobProcessorBuilder;
import com.sabre.reader.ReaderFactory;

public class Main {

    private static JobProcessor anagramProcessor = new JobProcessorBuilder()
            .withProcessor(new SortWordProcessor())
            .withProcessor(new BuildWordMapProcessor())
            .withProcessor(new FindLongestAnagramProcessor())
            .withProcessor(new FindLongestSetOfAnagramsProcessor())
            .build();


    public static void main(String[] args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("Input file argument expected");
        }

        ContextProcessor context = new Main().process(args[0]);
        System.out.println(context.toString());
    }

    public ContextProcessor process(String file) {
        return new ReaderFactory().createReader(file).process(anagramProcessor);
    }

}
