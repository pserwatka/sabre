package com.sabre.anagram.context;

import com.sabre.processor.ContextProcessor;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class AnagramContext implements ContextProcessor {
    public static final int MIN_WORD_SIZE = 1;

    private Map<String, Set<String>> anagramsMap = new HashMap<>();
    private String sortedString;
    private String longestAnagram;
    private String longestSetOfAnagrams;

    public void setSortedString(String sortedString) {
        this.sortedString = sortedString;
    }

    public String getSortedString() {
        return sortedString;
    }

    public String getLongestAnagram() {
        return longestAnagram;
    }

    public void setLongestAnagram(String longestAnagram) {
        this.longestAnagram = longestAnagram;
    }

    public void addToMap(String key, String word) {
        if (!anagramsMap.containsKey(key)) {
            anagramsMap.put(key, new HashSet<>());
        }
        anagramsMap.get(key).add(word);
    }

    public Map<String, Set<String>> getAnagramsMap() {
        return Collections.unmodifiableMap(anagramsMap);
    }


    public String getLongestSetOfAnagrams() {
        return longestSetOfAnagrams;
    }

    public void setLongestSetOfAnagrams(String longestSetOfAnagrams) {
        this.longestSetOfAnagrams = longestSetOfAnagrams;
    }

    public boolean isAnagram() {
        return getAnagramsMap().get(getSortedString()).size() > MIN_WORD_SIZE;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AnagramContext{\n");
        sb.append("\tlongestAnagram='").append(longestAnagram).append('\'').append(" anagrams: \'").append(getAnagramsMap().get(longestAnagram)).append("\'\n");
        sb.append("\tlongestSetOfAnagrams='").append(longestSetOfAnagrams).append('\'').append(" anagrams: \'").append(getAnagramsMap().get(longestSetOfAnagrams)).append("\'\n");
        sb.append("\tanagramsFound='").append(getAnagrams()).append("\'\n");
        sb.append('}');
        return sb.toString();
    }

    public List<String> getAnagrams() {
        return anagramsMap.entrySet().stream()
                .filter(entry -> entry.getValue().size() > 1)
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());
    }
}
