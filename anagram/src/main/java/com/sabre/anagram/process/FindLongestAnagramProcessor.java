package com.sabre.anagram.process;

import com.sabre.anagram.context.AnagramContext;
import com.sabre.processor.Processor;

public class FindLongestAnagramProcessor implements Processor<AnagramContext> {

    @Override
    public void process(AnagramContext context, String word) {
        if (context.isAnagram()) {
            initLongestAnagram(context);
            updateLongestAnagram(context, word);
        }
    }

    private void updateLongestAnagram(AnagramContext context, String word) {
        if (context.getLongestAnagram().length() < word.length()) {
            context.setLongestAnagram(context.getSortedString());
        }
    }

    private void initLongestAnagram(AnagramContext context) {
        if (context.getLongestAnagram() == null || "".equals(context.getLongestAnagram())) {
            context.setLongestAnagram(context.getSortedString());
        }
    }

}
