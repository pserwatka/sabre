package com.sabre.anagram.process;

import com.sabre.anagram.context.AnagramContext;
import com.sabre.processor.Processor;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortWordProcessor implements Processor<AnagramContext> {

    @Override
    public void process(AnagramContext context, String word) {
        context.setSortedString(Stream.of(word.toLowerCase().split(""))
                .sorted()
                .collect(Collectors.joining()));

    }
}
