package com.sabre.anagram.process;

import com.sabre.anagram.context.AnagramContext;
import com.sabre.processor.Processor;

public class FindLongestSetOfAnagramsProcessor implements Processor<AnagramContext> {

    @Override
    public void process(AnagramContext context, String word) {
        if (context.isAnagram()) {
            initLongesSetOfAnagrams(context);
            updateLongestSetOfAnagrams(context);
        }
    }

    private void updateLongestSetOfAnagrams(AnagramContext context) {
        if (context.getAnagramsMap().get(context.getLongestSetOfAnagrams()).size() < context.getAnagramsMap().get(context.getSortedString()).size()) {
            context.setLongestSetOfAnagrams(context.getSortedString());
        }
    }

    private void initLongesSetOfAnagrams(AnagramContext context) {
        if (context.getLongestSetOfAnagrams() == null || "".equals(context.getLongestSetOfAnagrams())) {
            context.setLongestSetOfAnagrams(context.getSortedString());
        }
    }
}
