package com.sabre.anagram.process;

import com.sabre.anagram.context.AnagramContext;
import com.sabre.processor.Processor;

public class BuildWordMapProcessor implements Processor<AnagramContext> {

    @Override
    public void process(AnagramContext context, String word) {
        context.addToMap(context.getSortedString(), word);
    }
}
