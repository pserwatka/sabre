package com.sabre.processor;

public interface Processor<T extends ContextProcessor> {

    void process(T context, String word);

}
