package com.sabre.processor;

import java.util.LinkedList;
import java.util.List;

public class JobProcessor {

    private List<Processor> processors;

    public void startProcess(ContextProcessor contextProcessor, String word) {
        processors.forEach(processor -> processor.process(contextProcessor, word));
    }

    public static class JobProcessorBuilder {

        private List<Processor> processors = new LinkedList<>();

        public JobProcessorBuilder withProcessor(Processor processor) {
            this.processors.add(processor);
            return this;
        }

        public JobProcessor build() {
            JobProcessor jobProcessor = new JobProcessor();
            jobProcessor.processors = this.processors;
            return jobProcessor;
        }
    }
}
