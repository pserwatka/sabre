package com.sabre.processor;

import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class JobProcessorTest {

    private JobProcessor jobProcessor;

    @Test
    public void shouldExecuteAllProcesses() {
        //given
        ContextProcessor contextProcessor = new TestContext();
        Processor processorA = Mockito.mock(Processor.class);
        Processor processorB = Mockito.mock(Processor.class);
        jobProcessor = new JobProcessor.JobProcessorBuilder()
                .withProcessor(processorA)
                .withProcessor(processorB)
                .build();

        //when
        jobProcessor.startProcess(contextProcessor, "");

        //then
        verify(processorA, times(1)).process(contextProcessor, eq(anyString()));
        verify(processorB, times(1)).process(contextProcessor, eq(anyString()));
    }

    public static class TestContext implements ContextProcessor {

    }
}