package com.sabre;

import com.sabre.anagram.context.AnagramContext;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MainTest {

    private Main main = new Main();

    @Test
    public void shouldCalculateData() {
        //given
        String file = "words.txt";

        //when
        AnagramContext context = (AnagramContext) main.process(file);

        //then
        assertThat(context.getLongestSetOfAnagrams()).isEqualTo("abc");
        assertThat(context.getLongestAnagram()).isEqualTo("aacc");
        assertThat(context.getAnagrams()).contains("abc","aacc");
    }

}