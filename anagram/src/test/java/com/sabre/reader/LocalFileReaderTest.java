package com.sabre.reader;

import com.sabre.anagram.context.AnagramContext;
import com.sabre.processor.JobProcessor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class LocalFileReaderTest {

    private LocalFileReader localFileReader;
    private JobProcessor processor;
    private BufferedReader bufferedReader;

    @Before
    public void beforeTest() {
        localFileReader = spy(new LocalFileReader("pathToFile"));
        bufferedReader = mock(BufferedReader.class);
        processor = mock(JobProcessor.class);
    }

    @Test
    public void shouldCraeteAndProcessLocalFileReader() throws IOException {
        //given
        Mockito.doReturn(bufferedReader).when(localFileReader).createBufferedReader();
        given(bufferedReader.readLine()).willReturn("line1", "line2", "line3", null);


        //when
        localFileReader.process(processor);

        //then
        verify(processor, times(3)).startProcess(any(AnagramContext.class), anyString());
    }

}