package com.sabre.reader;

import com.sabre.anagram.context.AnagramContext;
import com.sabre.processor.JobProcessor;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class InternetFileReaderTest {
    private InternetFileReader internetFileReader;
    private JobProcessor processor;
    private BufferedReader bufferedReader;

    @Before
    public void beforeTest() {
        internetFileReader = spy(new InternetFileReader("urlToFile"));
        bufferedReader = mock(BufferedReader.class);
        processor = mock(JobProcessor.class);
    }

    @Test
    public void shouldCreateAndProcessInternetFileReader() throws IOException {
        //given
        doReturn(bufferedReader).when(internetFileReader).createBufferedReader();
        given(bufferedReader.readLine()).willReturn("line1", "line2", "line3", null);


        //when
        internetFileReader.process(processor);

        //then
        verify(processor, times(3)).startProcess(any(AnagramContext.class), anyString());
    }
}