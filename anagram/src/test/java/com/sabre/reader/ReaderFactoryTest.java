package com.sabre.reader;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ReaderFactoryTest {

    private ReaderFactory readerFactory = new ReaderFactory();

    @Test
    public void shouldCreateInternetReader() {
        //given
        String url = "http://example.com";

        //when
        Reader reader = readerFactory.createReader(url);

        //then
        assertThat(reader.getClass()).isAssignableFrom(InternetFileReader.class);
    }

    @Test
    public void shouldCreateLocalFileReader() {
        //given
        String pathToFile = "/path/to/real/file";

        //when
        Reader reader = readerFactory.createReader(pathToFile);

        //then
        assertThat(reader.getClass()).isAssignableFrom(LocalFileReader.class);
    }
}