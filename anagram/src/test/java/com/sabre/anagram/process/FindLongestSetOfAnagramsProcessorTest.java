package com.sabre.anagram.process;

import com.sabre.anagram.context.AnagramContext;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FindLongestSetOfAnagramsProcessorTest {

    private FindLongestSetOfAnagramsProcessor processor = new FindLongestSetOfAnagramsProcessor();

    @Test
    public void shouldUpdateLongestSetOfAnagrams() {
        //given
        AnagramContext context = new AnagramContext();
        context.setSortedString("abcd");
        context.addToMap("abcd","abcd");
        context.addToMap("abcd","abdc");
        context.addToMap("abc","abd");
        context.setLongestSetOfAnagrams("abc");

        //when
        processor.process(context, "abcd");

        //then
        assertThat(context.getLongestSetOfAnagrams()).isEqualTo("abcd");
    }

    @Test
    public void shouldNotUpdateLongestSetOfAnagrams() {
        //given
        AnagramContext context = new AnagramContext();
        context.setSortedString("abc");
        context.addToMap("abcd","abcd");
        context.addToMap("abcd","abdc");
        context.addToMap("abcd","adbc");
        context.addToMap("abc","abc");
        context.addToMap("abc","acb");
        context.setLongestSetOfAnagrams("abcd");

        //when
        processor.process(context, "abc");

        //then
        assertThat(context.getLongestSetOfAnagrams()).isEqualTo("abcd");
    }

    @Test
    public void shouldInitiateLongestSetOfAnagrams() {
        //given
        AnagramContext context = new AnagramContext();
        context.setSortedString("abcd");
        context.addToMap("abcd","abcd");
        context.addToMap("abcd","abdc");

        //when
        processor.process(context,"abcd");

        //then
        assertThat(context.getLongestSetOfAnagrams()).isEqualTo("abcd");
    }
}