package com.sabre.anagram.process;

import com.sabre.anagram.context.AnagramContext;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Sets;
import org.junit.Test;

import java.util.AbstractMap.SimpleEntry;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class BuildWordMapProcessorTest {

    private BuildWordMapProcessor buildWordMapProcessor = new BuildWordMapProcessor();

    @Test
    public void shouldAddNewWordToMap() {
        //given
        AnagramContext context = new AnagramContext();
        context.setSortedString("aal");

        //when
        buildWordMapProcessor.process(context, "ala");

        //then
        assertThat(context.getAnagramsMap()).contains(new SimpleEntry<String, Set<String>>("aal", Sets.newLinkedHashSet("ala")));
    }

}