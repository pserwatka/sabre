package com.sabre.anagram.process;

import com.sabre.anagram.context.AnagramContext;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SortWordProcessorTest {

    private SortWordProcessor sortWordProcessor = new SortWordProcessor();

    @Test
    public void shouldSortWord() {
        //given
        AnagramContext context = new AnagramContext();

        //when
        sortWordProcessor.process(context, "ala");

        //then
        assertThat(context.getSortedString()).isEqualTo("aal");
    }

}