package com.sabre.anagram.process;

import com.sabre.anagram.context.AnagramContext;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FindLongestAnagramProcessorTest {

    private FindLongestAnagramProcessor findLongestAnagramProcessor = new FindLongestAnagramProcessor();


    @Test
    public void shouldInitializeLongestAnagram() {
        //given
        AnagramContext context = new AnagramContext();
        context.setSortedString("abruz");
        context.addToMap("abruz", "abruz");
        context.addToMap("abruz", "burza");

        //when
        findLongestAnagramProcessor.process(context, "arbuz");

        //then
        assertThat(context.getLongestAnagram()).isEqualTo("abruz");
    }

    @Test
    public void shouldUpdateLongestAnagram() {
        //given
        AnagramContext context = new AnagramContext();
        context.setSortedString("abruz");
        context.addToMap("abruz", "abruz");
        context.addToMap("abruz", "burza");
        context.setLongestAnagram("aal");

        //when
        findLongestAnagramProcessor.process(context, "arbuz");

        //then
        assertThat(context.getLongestAnagram()).isEqualTo("abruz");
    }

    @Test
    public void shouldNotUpdateLongestAnagram() {
        //given
        AnagramContext context = new AnagramContext();
        context.setSortedString("abruz");
        context.addToMap("abruz", "abruz");
        context.addToMap("abruz", "burza");
        context.setLongestAnagram("longest");

        //when
        findLongestAnagramProcessor.process(context,"arbuz");

        //then
        assertThat(context.getLongestAnagram()).isEqualTo("longest");
    }
}