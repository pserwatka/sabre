package com.sabre.anagram.context;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AnagramContextTest {

    public AnagramContext anagramContext = new AnagramContext();

    @Test
    public void shouldDisplayAllData() {
        //given
        anagramContext.addToMap("a","a");
        anagramContext.addToMap("ab","ab");
        anagramContext.addToMap("ab","ba");
        anagramContext.addToMap("abc","abc");
        anagramContext.addToMap("abc","acb");
        anagramContext.addToMap("abc","cab");
        anagramContext.setLongestSetOfAnagrams("abc");
        anagramContext.setLongestAnagram("abc");

        //when
        String toString = anagramContext.toString();

        //then
        assertThat(toString).isEqualTo("AnagramContext{\n" +
                "\tlongestAnagram='abc' anagrams: '[acb, abc, cab]'\n" +
                "\tlongestSetOfAnagrams='abc' anagrams: '[acb, abc, cab]'\n" +
                "\tanagramsFound='[ab, abc]'\n" +
                "}");
    }

}